==============================
Cookiecutter Rscripts Template
==============================

`Cookiecutter`_ template for creating documentation on your `R`_ projects.
This template will setup basic structure to create your project a new
project, or it can also be used over an existing project.


* `Latest built documentation`_

.. _latest built documentation: http://cookiecutter-rscripts.readthedocs.io/en/latest/index.html
.. _cookiecutter: https://github.com/audreyr/cookiecutter
