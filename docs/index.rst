.. cookiecutter-rscripts documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cookiecutter-rscripts documentation!
===============================================

* `Source code on GitLab`_

This project aims at providing users of the `R statistical language`_
a template to produce documentation with the python `Sphinx`_
package.

.. _r statistical language: https://www.r-project.org/
.. _sphinx: http://www.sphinx-doc.org/en/stable/
.. _source code on gitlab: https://gitlab.com/ericdevost/cookiecutter-rscripts


Getting Started
---------------

.. toctree::
   :maxdepth: 2

   tutorial
   troubleshooting
   contributing

Basics
------

.. toctree::
   :maxdepth: 2

   prompts

Advanced Features
-----------------

.. toctree::
   :maxdepth: 2

   console_script_setup
   baked_project


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _cookiecutter: http://cookiecutter.readthedocs.io/en/latest/
