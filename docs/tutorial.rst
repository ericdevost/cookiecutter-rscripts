.. _tutorial:

========
Tutorial
========

This tutorial will get you started in using this
template to create a Sphinx documentation for an `R project`_.


.. _r project: https://cran.r-project.org/

Step 1: Setup a virtual environment
-----------------------------------

You can use your system's python to start your documentation project
or use a ``virtualenv``. This tutorial will use the ``virtualenv``
method. You can consult the `python guide to virtualenv`_ to
read more about python virtualenvs and their virtues.

.. _python guide to virtualenv: http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/


To install virtualenv, simply install the python module::

   python -m pip install virtualenv --user


Once virtualenv have been installed, you can use your favorite method
to create your virtual environment, or create one for your project
like this::

    virtualenv -p python2.7 ~/.virtualenvs/your_rproject

Here, ``your_rproject`` is the name of the project that
you will create. Then, you can activate your virtual environment::

    source ~/.virtualenvs/serverdoc/bin/activate

You can got back anytime to the system python by typing::

  deactivate


On Windows, you can activate your virtual environment like this.

.. code-block:: powershell

    > \path\to\env\Scripts\activate

You may find that using a Command Prompt window works better
than gitbash.

Step 2: Install cookiecutter
----------------------------

On Linux, Windows and MacOS, once your have activated your
virtualenv, you can simply install cookiecutter with pip::

    pip install cookiecutter


Step 3: Generate your project
-----------------------------

Now it's time to generate your documentation skeleton for your
``R project``. To do so, use cookiecutter, pointing it at the
cookiecutter-rscript repo::

    cookiecutter https://gitlab.com/ericdevost/cookiecutter-rscript.git

.. note::

   If you want to use cookiecutter-rscript over an existing project,
   you can add the ``-f`` flag to the above command, meaning to
   not fail if the project exist.

You'll be asked to enter values to set your project up.
If you don't know what to enter, stick with the defaults. You can find
a detailed documentation on the values in the :ref:`prompts` section of
this documentation.

Once cookiecutter has completed, you will find a newly created
folder named after the value you entered in cookiecutter for the
*project name* field.


Step 3: Build your rproject project locally
------------------------------------------

Once your project had been set up with cookiecutter, you
can install the needed dependencies to build the documentation.

.. code-block:: console

   $ cd ~/your-rproject
   $ python -m pip install -r requirements/_docs.txt


You are now ready to build your documentation locally with
one simple command:

.. code-block:: console

  $ python setup.py docs

Your newly built documentation will be accessible in the
``build/docs/html`` folder.

Step 4: Write your doc
----------------------

Make any changes, corrections or additions to your documentation.
You can build it anytime by typing ``python setup.py docs``
and see the changes in your browser. Once you are satisfied, it's
time to publish your documentation.


Step 5: Create a GitLab Repo
----------------------------

Go to your GitLab account and create a new repository. Name it after
``your R project``, where ``your R project`` ideally matches
the ``project_name`` you entered when you ran cookiecutter.


Step 6: Activate your GitLab repo
---------------------------------

On your computer, enter your newly created ``project folder``, where
``project folder`` is the ``project_name`` you entered when you ran
cookiecutter, then activate your repository::

    cd ``~/project_name``
    git init .
    git add .
    git commit -m "Initial skeleton."
    git remote add origin your-gitlab-repo
    git push -u origin master


Step 8: Build and publish your doc on `Read the docs`_
-------------------------------------------------------

You can create an account on Read the docs to automatically
build your documentation hosted on GitLab.

.. _read the docs: https://readthedocs.org/
